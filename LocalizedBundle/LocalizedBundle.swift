//
//  LocalizedBundle.swift
//  BonnieDraw
//
//  Created by Professor on 08/11/2017.
//  Copyright © 2017 Professor. All rights reserved.
//

import UIKit

public class LocalizedBundle: Bundle {
    private struct AssociatedKeys {
        static var key = "LocalizedMainBundle"
    }

    public var isRightToLeft: Bool {
        guard let languageCode = Locale.current.languageCode else {
            return false
        }
        return Locale.characterDirection(forLanguage: languageCode) == .rightToLeft
    }

    override public func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        if let bundle = objc_getAssociatedObject(self, &AssociatedKeys.key) as? Bundle {
            return bundle.localizedString(forKey: key, value: value, table: tableName)
        }
        return super.localizedString(forKey: key, value: value, table: tableName)
    }

    static public func setPreferredLanguage(languageIdentifier: String) -> Bundle? {
        if let path = Bundle.main.path(forResource: languageIdentifier, ofType: "lproj") ?? Bundle.main.path(forResource: "Base", ofType: "lproj"),
           let bundle = Bundle(path: path) {
            UserDefaults.standard.set(languageIdentifier, forKey: "languageIdentifier")
            objc_setAssociatedObject(Bundle.main, &AssociatedKeys.key, bundle, .OBJC_ASSOCIATION_RETAIN)
            NotificationCenter.default.post(name: NSLocale.currentLocaleDidChangeNotification, object: Locale.current)
            return bundle
        }
        return nil
    }

    static public func clearInAppOverrides() {
        UserDefaults.standard.removeObject(forKey: "languageIdentifier")
        objc_setAssociatedObject(Bundle.main, &AssociatedKeys.key, nil, .OBJC_ASSOCIATION_RETAIN)
        NotificationCenter.default.post(name: NSLocale.currentLocaleDidChangeNotification, object: Locale.current)
    }

    static public func localize() {
        let _ = AppLocale.shared
        NSLocale.swizzle(selector: #selector(getter: NSLocale.current), with: #selector(getter: NSLocale.appLocale))
        NSLocale.swizzle(selector: #selector(getter: NSLocale.preferredLanguages), with: #selector(getter: NSLocale.appPreferredLanguages))
        object_setClass(Bundle.main, LocalizedBundle.self)
        if let languageIdentifier = UserDefaults.standard.string(forKey: "languageIdentifier"),
           let bundle = setPreferredLanguage(languageIdentifier: languageIdentifier) {
            objc_setAssociatedObject(Bundle.main, &AssociatedKeys.key, bundle, .OBJC_ASSOCIATION_RETAIN)
        }
    }
}
