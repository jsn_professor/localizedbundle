//
//  AppLocale.swift
//  iOSTemplate
//
//  Created by Professor on 2019/1/19.
//  Copyright © 2019 Agrowood. All rights reserved.
//

import UIKit

class AppLocale {
    static let shared = AppLocale()
    let originalLocale: Locale
    let originalPreferredLanguages: [String]

    init() {
        originalLocale = Locale.current
        originalPreferredLanguages = Locale.preferredLanguages
    }
}
