//
//  NSLocaleExtension.swift
//  iOSTemplate
//
//  Created by Professor on 2019/1/19.
//  Copyright © 2019 Agrowood. All rights reserved.
//

import UIKit

extension NSLocale {
    @objc class var appLocale: Locale {
        return Locale(identifier: UserDefaults.standard.string(forKey: "languageIdentifier") ?? AppLocale.shared.originalLocale.identifier)
    }

    @objc class var appPreferredLanguages: [String] {
        var languages = AppLocale.shared.originalPreferredLanguages
        if let languageIdentifier = UserDefaults.standard.string(forKey: "languageIdentifier") {
            if let index = languages.firstIndex(where: { $0.starts(with: languageIdentifier) }) {
                if index != 0 {
                    languages.remove(at: index)
                    languages.insert(languageIdentifier, at: 0)
                }
            } else {
                languages.insert(languageIdentifier, at: 0)
            }
        }
        return languages
    }

    static func swizzle(selector: Selector, with replacement: Selector) {
        let originalSelector = selector
        let swizzledSelector = replacement
        if let originalMethod = class_getClassMethod(self, originalSelector),
           let swizzledMethod = class_getClassMethod(self, swizzledSelector) {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
}
